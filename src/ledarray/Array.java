package ledarray;

import java.util.ArrayList;
import processing.core.PApplet;
import processing.core.PImage;

/** 
 * This is a Processing class that simulates low-resolution LED displays.
 * It contains set of two nested ArrayList's of LED's.
 * 
 * @author Pete Eigel
 * @see LED
 */

public class Array
{
    private PApplet parent;
    private int height;
    private int width;
    
    private ArrayList<ArrayList> rows = new ArrayList<>();
    
    private int paddingX;
    private int paddingY;
    private int radius;
    
    /**
     * @param parent
     * PApplet: the Processing sketch that the array is drawn upon.
     * @param columns
     * Number of LED's across
     * @param rows
     * Number of LED's down
     * @param LEDsize
     * Diameter of LED in pixels
     */
    public Array(PApplet parent, int columns, int rows, int LEDsize)
    {
        this.parent = parent;
        this.height = rows;
        this.width = columns;
        
        radius = LEDsize/2;
        paddingX = (parent.width - 2* width * radius)/(2*width);
        paddingY = (parent.height - 2* height * radius)/(2*height);
        
        
        for(int y = paddingY+radius; y<parent.height; y+=2*radius + 2*paddingY)
        {
            this.rows.add(new ArrayList<LED>());
            for(int x = paddingX+radius; x<parent.width; x+=2*radius + 2*paddingX)
            {
                this.rows.get(this.rows.size()-1).add(
                        new LED(parent, radius, x, y)
                        );
            }
        }
    }
    
    /**
     * Assumes LED's should evenly fill screen
     * 
     * @param parent
     * @param LEDsize 
     */
    public Array(PApplet parent, int LEDsize)
    {
        this(parent, parent.width/LEDsize, parent.height/LEDsize, LEDsize);
    }
    
    /**
     * Called in the draw() method of the Processing sketch. Renders array.
     */
    public void draw()
    {
        for(ArrayList<LED> row : rows)
            for(LED l : row)
                l.draw();
    }
    
    public LED get(int x, int y)
    {
        return (LED) rows.get(y).get(x);
    }
    
    /**
     * Sets the color of a single LED
     * @param x
     * x coordinate of LED
     * @param y
     * y coordinate of LED
     * @param r
     * Red component. 0-255
     * @param g
     * Green component 0-225
     * @param b 
     * Blue component 0-255
     */
    public void setColor(int x, int y, int r, int g, int b)
    {
        LED led = get(x,y);
        led.r = r;
        led.g = g;
        led.b = b;
    }
    
    /**
     * Sets the color of every LED in array
     * @param r
     * @param g
     * @param b  
     */
    public void setColor(int r, int g, int b)
    {
        for(ArrayList<LED> row : rows)
            for(LED led : row)
            {
                led.r = r;
                led.g = g;
                led.b = b;
            }
    }

    /**
     * Sets the color of an LED according to the average color of an image.
     * @param x
     * X coordinate of LED
     * @param y
     * Y coordinate of LED
     * @param img
     * Image to get color from
     */
    public void setColor(int x, int y, PImage img)
    {
        int red = 0;
        int blue = 0;
        int green = 0;
        
        for(int color : img.pixels)
        {
            red += color >> 16 & 0xFF;
            green += color >> 8 & 0xFF;
            blue += color & 0xFF;
        }
        
        red = red / img.pixels.length;
        blue = blue / img.pixels.length;
        green = green / img.pixels.length;
        
        setColor(x, y, red, green, blue);
    }
    
    /**
     * Fills array to match an image
     * @param img 
     * PImage to clone.
     */
    public void drawImage(PImage img)
    {
        int chunkW = img.width/width;
        int chunkH = img.height/height;
        
        if(chunkW > chunkH)
            chunkW = chunkH;
        else
            chunkH = chunkW;
        
       for(int y = 0; y < height; y++)
           for(int x = 0; x < width; x++)
               setColor(x, y, img.get(x*chunkW, y*chunkH, chunkH, chunkW));
    }
    
    /**
     * Set the minimum brightness of a single LED
     * @param x
     * @param y
     * @param min 
     */
    public void setMin(int x, int y, int min)
    {
        get(x,y).min = min;
    }
    
    /**
     * Set maximum brightness of a single LED
     * @param x
     * @param y
     * @param max 
     */
    public void setMax(int x, int y, int max)
    {
        get(x,y).max = max;
    }
    
    /**
     * Set the minimum brightness of the entire array.
     * @param min 
     */
    public void setMin(int min)
    {
        for(ArrayList<LED> row : rows)
            for(LED l : row)
                l.min = min;
    }
    /**
     * Set maximum brightness for the entire array
     * @param max 
     */
    public void setMax(int max)
    {
        for(ArrayList<LED> row : rows)
            for(LED l : row)
                l.max = max;
    }
}