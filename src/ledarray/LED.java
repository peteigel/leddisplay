package ledarray;

import processing.core.PApplet;

/**
 * A simple Class to render individual LED's
 * @author peteeigel
 */
public class LED
{
    private PApplet parent;
    
    /**
     * Red component
     */
    public int r = 0;
    /**
     * Green component
     */
    public int g = 0;
    /**
     * Blue component
     */
    public int b = 0;
   
    /**
     * Maximum brightness
     */
    public int max = 255;
    /**
     * Minimum brightness
     */
    public int min = 0;
    
    private int x;
    private int y;
    private int radius;
    
    /**
     * Creates an LED
     * @param parent
     * @param radius
     * @param x
     * @param y 
     */
    public LED(PApplet parent, int radius, int x, int y)
    {
        this.parent = parent;
        this.radius = radius;
        this.x = x;
        this.y = y;
    }
    /**
     * Renders LED. Called by Array.draw()
     */
    public void draw()
    {
        
        int trueR = (r*(max-min)/255)+min;
        int trueG = (g*(max-min)/255)+min;
        int trueB = (b*(max-min)/255)+min;
        
        if(trueR > 255)
            trueR = 255;
        if(trueG > 255)
            trueG = 255;
        if(trueB > 255)
            trueR = 255;
        if(trueR < 0)
            trueR = 0;
        if(trueG < 0)
            trueG = 0;
        if(trueB < 0)
            trueB = 0;
        if(min>max)
        {
            min = 0;
            max = 255;
        }
        
        parent.fill(trueR, trueG, trueB);
        parent.rectMode(PApplet.RADIUS);
        parent.rect(x, y, radius, radius);
    }
}