package main;

import ledarray.Array;
import processing.core.PApplet;
import sound.LevelListener;

public class LEDDisplay extends PApplet {
    public static void main(String[] args)
    {
        String[] processingArgs = new String[args.length + 1];
        
        for(int i = 0; i < args.length; i++)
            processingArgs[i] = args[i];
        
        processingArgs[processingArgs.length - 1] = "main.LEDDisplay";        
        PApplet.main(processingArgs);
    }
    
    public Array display;
    public LevelListener listener;
    
    float sens = 10;
    
    @Override
    public void setup()
    {
        size(screenWidth, screenHeight, P2D);
        
        display = new Array(this, 10);
        display.drawImage(loadImage("ledarray\\dark-night.jpg"));
        
        
        listener = new LevelListener(this);
        listener.start();
        
        frameRate(60);
        noSmooth();
        noCursor();
    }
    
    @Override
    public void draw()
    {
        display.setMax((int)(listener.get()*255*sens));
        listener.reset();
        background(0);
        display.draw();
        
        sens = ((float)mouseY/height)*10;
    }
    
    @Override
    public void stop()
    {
        listener.end();
        super.stop();
    }
    
    @Override
    public void mouseClicked()
    {
        System.out.println(frameRate);
    }
}