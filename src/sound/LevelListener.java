package sound;

import ddf.minim.AudioInput;
import ddf.minim.Minim;
import processing.core.PApplet;

public class LevelListener extends Thread
{
    private AudioInput input;
    private Minim minim;
    
    private float last = 0;
    private float sum = 0;
    private int count = 0;
    
    
    public LevelListener(PApplet parent)
    {
        super();
        minim = new Minim(parent);
        input = minim.getLineIn();
    }
    
    @Override
    public void run()
    {
        while(true)
        {
            try{
                this.sleep(10);
            }
            
            catch(Exception e){}
            
            last = input.mix.level();
            sum += last;
            count++;
        }
    }
    
    public float get()
    {
        return sum/count;
    }
    
    public void reset()
    {
        sum = 0;
        count = 0;
    }
    
    public void end()
    {
        input.close();
        minim.stop();
        this.stop();
    }
}